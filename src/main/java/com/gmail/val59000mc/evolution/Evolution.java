package com.gmail.val59000mc.evolution;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.evolution.callbacks.EvoCallbacks;
import com.gmail.val59000mc.evolution.listeners.EvoBlocksListener;
import com.gmail.val59000mc.evolution.listeners.EvoDamageListener;
import com.gmail.val59000mc.evolution.listeners.EvoDropListener;
import com.gmail.val59000mc.evolution.listeners.EvoForbiddenZoneListener;
import com.gmail.val59000mc.evolution.listeners.EvoMySQLListener;
import com.gmail.val59000mc.evolution.listeners.EvoVillagerListener;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;

public class Evolution extends JavaPlugin {
	
	public void onEnable(){
		this.getDataFolder().mkdirs();
		File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(),"config.yml"));
		File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(),"lang.yml"));
		
		HCGameAPI game = new HCGame.Builder("Evolution", this, config, lang)
				.withPluginCallbacks(new EvoCallbacks())
				.withDefaultListenersAnd(Sets.newHashSet(
					new EvoBlocksListener(),
					new EvoDropListener(),
					new EvoForbiddenZoneListener(),
					new EvoMySQLListener(),
					new EvoVillagerListener(),
					new EvoDamageListener()
				))
				.build();
		game.loadGame();
	}
	public void onDisable(){
	}
}
