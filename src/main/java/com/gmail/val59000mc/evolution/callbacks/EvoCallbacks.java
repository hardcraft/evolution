package com.gmail.val59000mc.evolution.callbacks;

import com.gmail.val59000mc.evolution.common.EvoConstants;
import com.gmail.val59000mc.evolution.common.EvoItems;
import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.evolution.players.EvoTeam;
import com.gmail.val59000mc.evolution.zones.ForbiddenZone;
import com.gmail.val59000mc.evolution.zones.KillZone;
import com.gmail.val59000mc.evolution.zones.PushZone;
import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.common.Constants;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.spigotutils.*;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.google.common.collect.Lists;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class EvoCallbacks extends DefaultPluginCallbacks {

    private ForbiddenZone redZone;
    private ForbiddenZone blueZone;


    public ForbiddenZone getRedZone() {
        return redZone;
    }

    public ForbiddenZone getBlueZone() {
        return blueZone;
    }

    @Override
    public HCTeam newHCTeam(String name, ChatColor color, Location spawnpoint) {
        return new EvoTeam(name, color, spawnpoint);
    }

    @Override
    public HCPlayer newHCPlayer(Player player) {
        return new EvoPlayer(player);
    }

    @Override
    public HCPlayer newHCPlayer(HCPlayer hcPlayer) {
        return new EvoPlayer(hcPlayer);
    }

    @Override
    public List<HCTeam> createTeams() {
        World world = getApi().getWorldConfig().getWorld();

        List<HCTeam> teams = new ArrayList<HCTeam>();
        EvoTeam redTeam = (EvoTeam) newHCTeam("Rouge", ChatColor.RED, Parser.parseLocation(world, getConfig().getString("teams.red.spawnpoint")));
        EvoTeam blueTeam = (EvoTeam) newHCTeam("Bleu", ChatColor.BLUE, Parser.parseLocation(world, getConfig().getString("teams.blue.spawnpoint")));
        loadForbiddenZones(world, blueTeam, redTeam);
        teams.add(redTeam);
        teams.add(blueTeam);
        return teams;
    }

    @Override
    public List<Stuff> createStuffs() {

        return Lists.newArrayList(
            new Stuff.Builder("Rouge")
                .addArmorItems(EvoItems.getArmor(Color.fromRGB(255, 0, 0)))
                .addInventoryItems(EvoItems.getItems())
                .build(),
            new Stuff.Builder("Bleu")
                .addArmorItems(EvoItems.getArmor(Color.fromRGB(0, 0, 255)))
                .addInventoryItems(EvoItems.getItems())
                .build()
        );

    }

    private void loadForbiddenZones(World world, EvoTeam blueTeam, EvoTeam redTeam) {
        ConfigurationSection redPushSection = getApi().getConfig().getConfigurationSection("teams.red.beacon-barrier");
        ConfigurationSection redKillSection = getApi().getConfig().getConfigurationSection("teams.red.kill-zone");
        redZone = getForbiddenZone(world, redPushSection, redKillSection, blueTeam);

        ConfigurationSection bluePushSection = getApi().getConfig().getConfigurationSection("teams.blue.beacon-barrier");
        ConfigurationSection blueKillSection = getApi().getConfig().getConfigurationSection("teams.blue.kill-zone");
        blueZone = getForbiddenZone(world, bluePushSection, blueKillSection, redTeam);
    }

    private ForbiddenZone getForbiddenZone(World world, ConfigurationSection pushSection, ConfigurationSection killSection, HCTeam hcTeam) {

        List<PushZone> pushZones = new ArrayList<PushZone>();
        if (pushSection != null) {
            for (String key : pushSection.getKeys(false)) {
                ConfigurationSection zoneSection = pushSection.getConfigurationSection(key);
                Location min = Parser.parseLocation(world, zoneSection.getString("min"));
                Location max = Parser.parseLocation(world, zoneSection.getString("max"));
                Vector push = Parser.parseVector(zoneSection.getString("push-ennemy"));
                LocationBounds bounds = new LocationBounds(min, max);
                pushZones.add(new PushZone(bounds, push));
            }
        }

        List<KillZone> killZones = new ArrayList<KillZone>();
        if (killSection != null) {
            for (String key : killSection.getKeys(false)) {
                ConfigurationSection zoneSection = killSection.getConfigurationSection(key);
                Location min = Parser.parseLocation(world, zoneSection.getString("min"));
                Location max = Parser.parseLocation(world, zoneSection.getString("max"));
                LocationBounds bounds = new LocationBounds(min, max);
                killZones.add(new KillZone(bounds));
            }
        }

        return new ForbiddenZone(getApi(), pushZones, killZones, hcTeam);
    }

    @Override
    public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {

        List<String> content = new ArrayList<String>();

        // All teams kills
        content.add(getStringsApi().get("messages.scoreboard.scores").toString());
        for (HCTeam team : getPmApi().getTeams()) {
            content.add(" " + team.getColor() + team.getName() + " " + ChatColor.WHITE + team.getKills());
        }

        // Remaining time (if exists)
        if (getApi().is(GameState.PLAYING) && getConfig().getBoolean("config.countdown-end-of-game.enable", Constants.DEFAULT_COUNTDOWN_ENABLED)) {
            int remainingTime = getConfig().getInt("config.live.countdown", 0);
            content.add(
                getStringsApi().get("messages.scoreboard.remaining-time-before-end").toString() +
                    " " +
                    ChatColor.GREEN + Time.getFormattedTime(remainingTime)
            );
        }

        // Kills
        content.add(getStringsApi().get("messages.scoreboard.kills-deaths").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getKills() + ChatColor.WHITE + "/" + ChatColor.GREEN + hcPlayer.getDeaths());

        // Stars
        content.add(getStringsApi().get("evo.scoreboard.stars").toString());
        content.add(" " + ChatColor.GREEN + "" + ((EvoPlayer) hcPlayer).getStars());

        // Coins
        content.add(getStringsApi().get("messages.scoreboard.coins").toString());
        content.add(" " + ChatColor.GREEN + "" + hcPlayer.getMoney());

        // Teammate
        content.add(getStringsApi().get("messages.scoreboard.camping").toString());
        content.add(" " + getAnticampProgressBar((EvoPlayer) hcPlayer));

        return content;
    }

    private String getAnticampProgressBar(EvoPlayer evoPlayer) {
        long progress = (long) Math.ceil(100.0 * (double) evoPlayer.getCampingSeconds() / (double) EvoConstants.MAX_CAMPING_SECONDS);
        long progressDecade = progress / 10;
        StringBuilder builder = new StringBuilder();

        ChatColor progressColor = ChatColor.GREEN;
        if (progress >= 75)
            progressColor = ChatColor.RED;
        else if (progress >= 50)
            progressColor = ChatColor.GOLD;
        else if (progress >= 25)
            progressColor = ChatColor.YELLOW;

        builder.append(progressColor);
        for (long i = 0; i < progressDecade; i++) {
            builder.append("|");
        }

        builder.append(ChatColor.GRAY);
        for (long i = progressDecade; i < 10; i++) {
            builder.append("|");
        }

        builder.append(" ");
        builder.append(progressColor);
        builder.append(progress);
        builder.append("%");

        return builder.toString();
    }


    @Override
    public void startPlayer(HCPlayer hcPlayer) {
        super.startPlayer(hcPlayer);

        if (hcPlayer.isOnline()) {

            Player player = hcPlayer.getPlayer();
            player.setLevel(1000);
            player.setGameMode(GameMode.ADVENTURE);

            Effects.addPermanent(player, PotionEffectType.SPEED, 0, false);
            Effects.addPermanent(player, PotionEffectType.SATURATION, 0, false);
        }
    }

    @Override
    public void waitPlayerAtLobby(HCPlayer hcPlayer) {
        super.waitPlayerAtLobby(hcPlayer);
        if (hcPlayer.isOnline()) {
            Player player = hcPlayer.getPlayer();
            player.setGameMode(GameMode.ADVENTURE);
        }
    }

    @Override
    public void assignStuffToPlayer(HCPlayer hcPlayer) {
        if (hcPlayer.getStuff() == null && hcPlayer.hasTeam()) {
            hcPlayer.setStuff(getItemsApi().getStuff(hcPlayer.getTeam().getName()));
        }
    }

    @Override
    public void respawnPlayer(HCPlayer hcPlayer) {
        if (hcPlayer.isOnline()) {
            hcPlayer.getScoreboard().update();
            Player player = hcPlayer.getPlayer();
            player.setGameMode(GameMode.ADVENTURE);
            Effects.addPermanent(player, PotionEffectType.SPEED, 0, false);
            Effects.addPermanent(player, PotionEffectType.SATURATION, 0, false);
            ((EvoPlayer) hcPlayer).resetLastPVP();
        }
    }

    private boolean isStarsDoubled(EvoPlayer evoPlayer) {
        return Randoms.randomInteger(1, 100) <= evoPlayer.getDoubleStarsChance();
    }

    public void rewardStars(Player bukkitPlayer, EvoPlayer evoPlayer, int amount, boolean playSoundAndMessageIfDoubled) {

        if (bukkitPlayer == null)
            return;

        int rewardedAmount = amount;

        if (isStarsDoubled(evoPlayer)) {
            rewardedAmount = amount * 2;
            if (playSoundAndMessageIfDoubled) {
                getStringsApi()
                    .get("evo.double-star-amount")
                    .sendChatP(evoPlayer);
                getSoundApi().play(evoPlayer, Sound.ENTITY_VILLAGER_YES, 1, 2f);
            }
        }

        evoPlayer.addStars(rewardedAmount);
        Log.debug("Rewarded " + rewardedAmount + " stars to " + evoPlayer.getName());
        bukkitPlayer
            .getInventory()
            .addItem(new ItemStack(Material.NETHER_STAR, rewardedAmount));

        getStringsApi()
            .get("evo.rewarded-stars")
            .replace("%amount%", String.valueOf(rewardedAmount))
            .sendActionBar(evoPlayer);
    }

    @Override
    public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {

        event.setKeepInventory(true);
        event.setKeepLevel(true);
        event.setDroppedExp(0);
        event.getDrops().clear();
        ((EvoPlayer) hcKilled).resetLastPVP();

        getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());

    }

    @Override
    public void handleKillEvent(PlayerDeathEvent e, HCPlayer hcKilled, HCPlayer hcKiller) {

        // ignore event if not playing players
        if (!hcKilled.is(PlayerState.PLAYING) || !hcKiller.is(PlayerState.PLAYING) || !hcKilled.hasTeam() || !hcKiller.hasTeam()) {
            return;
        }

        getPmApi().rewardMoneyTo(hcKiller, getKillReward(hcKiller));

        EvoPlayer evoKiller = (EvoPlayer) hcKiller;
        EvoPlayer evoKilled = (EvoPlayer) hcKilled;

        e.setKeepInventory(true);
        e.getDrops().clear();
        e.setKeepLevel(true);
        e.setDroppedExp(0);

        evoKilled.resetLastPVP();

        // reward killed player
        double balancedKilledRewardPercentage = calculateBalancedRewardPercentage(evoKilled.getTeam().getKills(), getOtherTeam((EvoTeam) evoKilled.getTeam()).getKills());
        int balancedKilledReward = calculateBalancedStarsReward(EvoConstants.STARS_KILLED, balancedKilledRewardPercentage);
        rewardStars(e.getEntity(), (EvoPlayer) hcKilled, balancedKilledReward, false);

        // reward killer and his team
        double balancedKillRewardPercentage = 100d;
        int balancedKillReward = EvoConstants.STARS_KILL + EvoConstants.STARS_TEAM_KILL;
        int balancedTeamKillReward = EvoConstants.STARS_TEAM_KILL;
        if (balancedKilledRewardPercentage == 100d) {
            balancedKillRewardPercentage = calculateBalancedRewardPercentage(evoKiller.getTeam().getKills(), getOtherTeam((EvoTeam) evoKiller.getTeam()).getKills());
            balancedKillReward = calculateBalancedStarsReward(balancedKillReward, balancedKillRewardPercentage);
            balancedTeamKillReward = calculateBalancedStarsReward(balancedTeamKillReward, balancedKillRewardPercentage);
        }

        for (HCPlayer teammate : evoKiller.getTeam().getOtherMembers(evoKiller)) {
            rewardStars(teammate.getPlayer(), (EvoPlayer) teammate, balancedTeamKillReward, false);
        }
        rewardStars(e.getEntity().getKiller(), (EvoPlayer) evoKiller, balancedKillReward, true);

        checkIfTeamHasWon(evoKiller.getTeam());

        getPmApi().autoRespawnPlayerAfter20Ticks(e.getEntity());

    }

    /**
     * Calculate reward balance bonus according to teams scores
     *
     * @param rewardedTeamKills
     * @param otherTeamKills
     * @return
     */
    private double calculateBalancedRewardPercentage(int rewardedTeamKills, int otherTeamKills) {
        int retard = otherTeamKills - rewardedTeamKills;
        double percentage = 100d;
        if (retard < 5)
            return percentage; // no bonus

        int multipleOfFive = retard - retard % 5;
        percentage = percentage + 3 * multipleOfFive; // +15% by 5 kills retard mutiple

        // capped at +75%
        if (percentage > 175d) {
            percentage = 175d;
        }

        Log.debug("Kills difference of " + multipleOfFive + " resulting in a bonus of " + percentage + "%");

        return percentage;
    }

    /**
     * Calculate stars reward after balanced reward percentage
     *
     * @param originalReward
     * @param balancedKilledRewardPercentage
     * @return
     */
    private int calculateBalancedStarsReward(int originalReward, double balancedKilledRewardPercentage) {
        return (int) Math.ceil((double) originalReward * balancedKilledRewardPercentage / 100d);
    }

    private EvoTeam getOtherTeam(EvoTeam team) {
        if (team.getName().equals("Rouge")) {
            return (EvoTeam) getPmApi().getHCTeam("Bleu");
        } else {
            return (EvoTeam) getPmApi().getHCTeam("Rouge");
        }
    }

    private void checkIfTeamHasWon(HCTeam team) {
        if (team.getKills() >= EvoConstants.KILLS_LIMIT) {
            getApi().endGame(team);
        }
    }

    @Override
    public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

        if (hcPlayer.isOnline()) {

            EvoPlayer evoPlayer = (EvoPlayer) hcPlayer;

            Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.winning-team")
                .replace("%game%", getApi().getName())
                .replace("%kills%", String.valueOf(hcPlayer.getKills()))
                .replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
                .replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
                .replace("%stars%", String.valueOf(evoPlayer.getStars()))
                .replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
                .replace("%winner%", (winningTeam == null ? "Match nul" : winningTeam.getColor() + winningTeam.getName()))
                .toString()
            );

        }

    }

}
