package com.gmail.val59000mc.evolution.common;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPlayersManagerAPI;
import com.gmail.val59000mc.hcgameslib.api.HCSoundAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.PotionMetaBuilder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import java.util.List;
import java.util.Map;

public class EvoInventories {

    private HCGameAPI api;

    private String price;
    private String required;
    private String keepEnchant;

    private String goldenApple;
    private String leap;
    private String speed;
    private String heal;
    private String damage;

    private String sword;
    private String bow;

    private String protection;
    private String power;
    private String featherFalling;
    private String sharpness;

    private String helmet;
    private String chestplate;
    private String leggings;
    private String boots;

    private String leather;
    private String wood;
    private String gold;
    private String stone;
    private String iron;
    private String diamond;

    private ChatColor green;
    private ChatColor red;

    public EvoInventories(HCGameAPI api) {
        this.api = api;
        HCStringsAPI s = api.getStringsAPI();

        price = s.get("evo.price").toString();
        required = s.get("evo.required").toString();
        keepEnchant = s.get("evo.keep-enchant").toString();

        goldenApple = s.get("evo.golden-apple").toString();
        leap = s.get("evo.leap").toString();
        speed = s.get("evo.speed").toString();
        heal = s.get("evo.heal").toString();
        damage = s.get("evo.damage").toString();

        sword = s.get("evo.sword").toString();
        bow = s.get("evo.bow").toString();

        protection = s.get("evo.protection").toString();
        power = s.get("evo.power").toString();
        featherFalling = s.get("evo.feather-falling").toString();
        sharpness = s.get("evo.sharpness").toString();

        helmet = s.get("evo.helmet").toString();
        chestplate = s.get("evo.chestplate").toString();
        leggings = s.get("evo.leggings").toString();
        boots = s.get("evo.boots").toString();

        leather = s.get("evo.leather").toString();
        wood = s.get("evo.wood").toString();
        gold = s.get("evo.gold").toString();
        stone = s.get("evo.stone").toString();
        iron = s.get("evo.iron").toString();
        diamond = s.get("evo.diamond").toString();

        green = ChatColor.GREEN;
        red = ChatColor.RED;
    }


    private HCPlayersManagerAPI getPmApi() {
        return api.getPlayersManagerAPI();
    }

    public HCStringsAPI getStringsApi() {
        return api.getStringsAPI();
    }

    public HCSoundAPI getSoundApi() {
        return api.getSoundAPI();
    }

    public Item getGlass(int position) {
        return new Item.ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).withDamage((short) 15).withPosition(position).build();
    }

    public Item getGoldenApple(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.GOLDEN_APPLE)
            .withName(green + goldenApple)
            .withLore(Lists.newArrayList(red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(starsAmount, null, new ItemStack(Material.GOLDEN_APPLE))));
    }

    public Item getLeapPotion(int position, int starsAmount) {
        PotionEffect effect = Iterables.getFirst(new Potion(PotionType.JUMP, 2).getEffects(), null);
        return new Item.ItemBuilder(Material.SPLASH_POTION)
            .addPotionEffect(effect)
            .withName(green + leap)
            .withLore(Lists.newArrayList(red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .withHideFlags(true)
            .build().setActions(Lists.newArrayList(new EvoExchangeItemAction(starsAmount, null, makePotion(effect))));
    }

    public Item getSpeedPotion(int position, int starsAmount) {
        PotionEffect effect = Iterables.getFirst(new Potion(PotionType.SPEED, 2).getEffects(), null);
        return new Item.ItemBuilder(Material.SPLASH_POTION)
            .addPotionEffect(effect)
            .withName(green + speed)
            .withLore(Lists.newArrayList(red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .withHideFlags(true)
            .build().setActions(Lists.newArrayList(new EvoExchangeItemAction(starsAmount, null, makePotion(effect))));
    }

    public Item getHealPotion(int position, int starsAmount) {
        PotionEffect effect = Iterables.getFirst(new Potion(PotionType.INSTANT_HEAL, 2).getEffects(), null);
        return new Item.ItemBuilder(Material.SPLASH_POTION)
            .addPotionEffect(effect)
            .withName(green + heal)
            .withLore(Lists.newArrayList(red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .withHideFlags(true)
            .build().setActions(Lists.newArrayList(new EvoExchangeItemAction(starsAmount, null, makePotion(effect))));
    }

    public Item getDamagePotion(int position, int starsAmount) {
        PotionEffect effect = Iterables.getFirst(new Potion(PotionType.INSTANT_DAMAGE, 2).getEffects(), null);
        return new Item.ItemBuilder(Material.SPLASH_POTION)
            .addPotionEffect(effect)
            .withName(green + damage)
            .withLore(Lists.newArrayList(red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .withHideFlags(true)
            .build().setActions(Lists.newArrayList(new EvoExchangeItemAction(starsAmount, null, makePotion(effect))));
    }

    private ItemStack makePotion(PotionEffect effect) {
        ItemBuilder item = new ItemBuilder(Material.SPLASH_POTION);
        PotionMetaBuilder potion = new PotionMetaBuilder(item);
        potion.withMainEffect(effect.getType());
        potion.withCustomEffect(effect, true);
        item.withMeta(potion);
        return item.build();
    }

    public Item getGoldHelmet(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.GOLDEN_HELMET)
            .withName(green + helmet + gold)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", helmet + leather),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.LEATHER_HELMET),
                new ItemBuilder(Material.GOLDEN_HELMET)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getGoldChestplate(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.GOLDEN_CHESTPLATE)
            .withName(green + chestplate + gold)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", chestplate + leather),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.LEATHER_CHESTPLATE),
                new ItemBuilder(Material.GOLDEN_CHESTPLATE)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getGoldLeggings(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.GOLDEN_LEGGINGS)
            .withName(green + leggings + gold)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", leggings + leather),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.LEATHER_LEGGINGS),
                new ItemBuilder(Material.GOLDEN_LEGGINGS)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getGoldBoots(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.GOLDEN_BOOTS)
            .withName(green + boots + gold)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", boots + leather),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.LEATHER_BOOTS),
                new ItemBuilder(Material.GOLDEN_BOOTS)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getIronHelmet(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.IRON_HELMET)
            .withName(green + helmet + iron)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", helmet + gold),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.GOLDEN_HELMET),
                new ItemBuilder(Material.IRON_HELMET)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getIronChestplate(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.IRON_CHESTPLATE)
            .withName(green + chestplate + iron)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", chestplate + gold),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.GOLDEN_CHESTPLATE),
                new ItemBuilder(Material.IRON_CHESTPLATE)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getIronLeggings(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.IRON_LEGGINGS)
            .withName(green + leggings + iron)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", leggings + gold),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.GOLDEN_LEGGINGS),
                new ItemBuilder(Material.IRON_LEGGINGS)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getIronBoots(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.IRON_BOOTS)
            .withName(green + boots + iron)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", boots + gold),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.GOLDEN_BOOTS),
                new ItemBuilder(Material.IRON_BOOTS)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getDiamondHelmet(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.DIAMOND_HELMET)
            .withName(green + helmet + diamond)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", helmet + iron),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.IRON_HELMET),
                new ItemBuilder(Material.DIAMOND_HELMET)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getDiamondChestplate(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.DIAMOND_CHESTPLATE)
            .withName(green + chestplate + diamond)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", chestplate + iron),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.IRON_CHESTPLATE),
                new ItemBuilder(Material.DIAMOND_CHESTPLATE)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getDiamondLeggings(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.DIAMOND_LEGGINGS)
            .withName(green + leggings + diamond)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", leggings + iron),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.IRON_LEGGINGS),
                new ItemBuilder(Material.DIAMOND_LEGGINGS)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getDiamondBoots(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.DIAMOND_BOOTS)
            .withName(green + boots + diamond)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", boots + iron),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.IRON_BOOTS),
                new ItemBuilder(Material.DIAMOND_BOOTS)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getStoneSword(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.STONE_SWORD)
            .withName(green + sword + stone)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", sword + wood),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.WOODEN_SWORD),
                new ItemBuilder(Material.STONE_SWORD)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getIronSword(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.IRON_SWORD)
            .withName(green + sword + iron)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", sword + stone),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.STONE_SWORD),
                new ItemBuilder(Material.IRON_SWORD)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }

    public Item getDiamondSword(int position, int starsAmount) {
        return new Item.ItemBuilder(Material.DIAMOND_SWORD)
            .withName(green + sword + diamond)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount)),
                red + required.replace("%required%", sword + iron),
                red + keepEnchant))
            .withPosition(position)
            .withUnbreakable(true)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                new ItemStack(Material.IRON_SWORD),
                new ItemBuilder(Material.DIAMOND_SWORD)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .item()
                    .build()))
            );
    }


    public Item getBow(int position, int starsAmount) {

        Map<Enchantment, Integer> ench = Maps.newHashMap();
        ench.put(Enchantment.ARROW_INFINITE, 1);

        return new Item.ItemBuilder(Material.BOW)
            .withName(green + bow)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .withUnbreakable(true)
            .withEnchantments(ench)
            .withHideFlags(true)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                starsAmount,
                null,
                new ItemBuilder(Material.BOW)
                    .buildMeta()
                    .withUnbreakable(true)
                    .withItemFlags(ItemFlag.HIDE_UNBREAKABLE)
                    .withEnchant(Enchantment.ARROW_INFINITE, 1, true)
                    .item()
                    .build()))
            );
    }

    public Item getProtection(int position, int starsAmount) {

        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);

        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();
        meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
        book.setItemMeta(meta);

        Map<Enchantment, Integer> ench = Maps.newHashMap();
        ench.put(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

        return new Item.ItemBuilder(Material.ENCHANTED_BOOK)
            .withEnchantments(ench)
            .withName(green + protection)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                    starsAmount,
                    null,
                    book
                ))
            );
    }

    public Item getSharpness(int position, int starsAmount) {

        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);

        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();
        meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 2, true);
        book.setItemMeta(meta);

        Map<Enchantment, Integer> ench = Maps.newHashMap();
        ench.put(Enchantment.DAMAGE_ALL, 2);

        return new Item.ItemBuilder(Material.ENCHANTED_BOOK)
            .withEnchantments(ench)
            .withName(green + sharpness)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                    starsAmount,
                    null,
                    book
                ))
            );
    }

    public Item getPower(int position, int starsAmount) {

        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);

        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();
        meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 2, true);
        book.setItemMeta(meta);

        Map<Enchantment, Integer> ench = Maps.newHashMap();
        ench.put(Enchantment.ARROW_DAMAGE, 2);

        return new Item.ItemBuilder(Material.ENCHANTED_BOOK)
            .withEnchantments(ench)
            .withName(green + power)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                    starsAmount,
                    null,
                    book
                ))
            );
    }

    public Item getFeatherFalling(int position, int starsAmount) {

        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);

        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();
        meta.addStoredEnchant(Enchantment.PROTECTION_FALL, 1, true);
        book.setItemMeta(meta);

        Map<Enchantment, Integer> ench = Maps.newHashMap();
        ench.put(Enchantment.PROTECTION_FALL, 1);

        return new Item.ItemBuilder(Material.ENCHANTED_BOOK)
            .withEnchantments(ench)
            .withName(green + featherFalling)
            .withLore(Lists.newArrayList(
                red + price.replace("%amount%", String.valueOf(starsAmount))))
            .withPosition(position)
            .build()
            .setActions(Lists.newArrayList(new EvoExchangeItemAction(
                    starsAmount,
                    null,
                    book
                ))
            );
    }


    private class EvoExchangeItemAction extends Action {

        private ItemStack requiredStars;
        private ItemStack requiredItem;
        private ItemStack sold;

        public EvoExchangeItemAction(int starAmount, ItemStack requiredItem, ItemStack sold) {
            super();
            this.requiredStars = new ItemStack(Material.NETHER_STAR, starAmount);
            this.requiredItem = requiredItem;
            this.sold = sold;
        }

        @Override
        public void executeAction(Player player, SigPlayer sigPlayer) {

            HCPlayer hcPlayer = getPmApi().getHCPlayer(player);

            if (hcPlayer != null && hcPlayer.isPlaying()) {

                if (!Inventories.containsAtLeast(player.getInventory(), requiredStars, requiredStars.getAmount(), new ItemCompareOption())) {
                    getStringsApi().get("evo.not-enough-stars").sendChatP(hcPlayer);
                    getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1, 2f);
                    executeNextAction(player, sigPlayer, false);
                    return;
                }


                Map<Enchantment, Integer> enchantments = null;
                if (requiredItem != null) {
                    List<ItemStack> itemsFound = Inventories.find(player.getInventory(), requiredItem, new ItemCompareOption());
                    if (itemsFound.size() == 0) {
                        getStringsApi().get("evo.not-required-item").sendChatP(hcPlayer);
                        getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 1, 2f);
                        executeNextAction(player, sigPlayer, false);
                        return;
                    } else {
                        ItemStack found = itemsFound.get(0);
                        enchantments = found.getEnchantments();
                        Inventories.remove(player.getInventory(), requiredItem, new ItemCompareOption());
                    }
                }
                Inventories.remove(player.getInventory(), requiredStars, new ItemCompareOption());


                ItemStack newItem = sold.clone();
                if (enchantments != null)
                    newItem.addUnsafeEnchantments(enchantments);
                player.getInventory().addItem(newItem);

                getSoundApi().play(hcPlayer, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 2);
                getStringsApi().get("evo.transaction-success").sendChatP(hcPlayer);
                executeNextAction(player, sigPlayer, true);

            } else {
                executeNextAction(player, sigPlayer, false);
            }

        }

    }
}
