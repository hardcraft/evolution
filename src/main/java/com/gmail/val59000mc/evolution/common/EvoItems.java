package com.gmail.val59000mc.evolution.common;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.LeatherArmorMetaBuilder;
import com.google.common.collect.Lists;

public class EvoItems {

	public static List<ItemStack> getArmor(Color color){
		return Lists.newArrayList(
			new ItemBuilder(Material.LEATHER_HELMET)
				.buildMeta(LeatherArmorMetaBuilder.class)
				.withColor(color)
				.withUnbreakable(true)
				.item()
				.build(),
				
			new ItemBuilder(Material.LEATHER_CHESTPLATE)
				.buildMeta(LeatherArmorMetaBuilder.class)
				.withColor(color)
				.withUnbreakable(true)
				.item()
				.build(), 
				
		    new ItemBuilder(Material.LEATHER_LEGGINGS)
				.buildMeta(LeatherArmorMetaBuilder.class)
				.withColor(color)
				.withUnbreakable(true)
				.item()
				.build(),
				
		    new ItemBuilder(Material.LEATHER_BOOTS)
				.buildMeta(LeatherArmorMetaBuilder.class)
				.withColor(color)
				.withUnbreakable(true)
				.item()
				.build()
		);
	}
	
	public static List<ItemStack> getItems(){
		return Lists.newArrayList(
			new ItemBuilder(Material.WOODEN_SWORD)
				.buildMeta()
				.withUnbreakable(true)
				.item()
				.build(),
				
			new ItemStack(Material.ARROW)
		);
	}
	
}
