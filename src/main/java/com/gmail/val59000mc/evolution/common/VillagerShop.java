package com.gmail.val59000mc.evolution.common;

import com.gmail.val59000mc.spigotutils.VillagerTradeApi;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;

import com.gmail.val59000mc.hcgameslib.players.HCTeam;

public class VillagerShop {
	
	private Villager potion;
	private Villager armor;
	private Villager weapon;
	private Villager enchant;
	
	private HCTeam team;
	
	
	
	public VillagerShop(Entity potion, Entity armor, Entity weapon, Entity enchant, HCTeam team) {
		super();
		this.potion = (Villager) potion;
		this.armor = (Villager) armor;
		this.weapon = (Villager) weapon;
		this.enchant = (Villager) enchant;
		this.team = team;
	}
	
	public Villager getPotion() {
		return potion;
	}
	public Villager getArmor() {
		return armor;
	}
	public Villager getWeapon() {
		return weapon;
	}
	public Villager getEnchant() {
		return enchant;
	}
	public HCTeam getTeam() {
		return team;
	}
	
	public void clearTrades(){
		VillagerTradeApi api = VillagerTradeApi.getAPI();
		api.clearTrades(potion);
		api.clearTrades(armor);
		api.clearTrades(weapon);
		api.clearTrades(enchant);
	}

	public void updateNames() {
		ChatColor c = team.getColor();
		potion.setCustomName(c+"Potions");
		armor.setCustomName(c+"Armures");
		weapon.setCustomName(c+"Armes");
		enchant.setCustomName(c+"Enchantements");
		
	}

	public void setProfessions() {
		potion.setProfession(Villager.Profession.CLERIC);
		armor.setProfession(Villager.Profession.ARMORER);
		weapon.setProfession(Villager.Profession.WEAPONSMITH);
		enchant.setProfession(Villager.Profession.LIBRARIAN);
	}

	public void setNoSound() {
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "data merge entity "+potion.getUniqueId().toString()+" {\"Silent\":1}");
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "data merge entity "+armor.getUniqueId().toString()+" {\"Silent\":1}");
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "data merge entity "+weapon.getUniqueId().toString()+" {\"Silent\":1}");
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "data merge entity "+enchant.getUniqueId().toString()+" {\"Silent\":1}");
		
	}
	
	
	
}
