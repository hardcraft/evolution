package com.gmail.val59000mc.evolution.listeners;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.craftbukkit.v1_14_R1.block.impl.CraftAnvil;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;

public class EvoBlocksListener extends HCListener{

	private Map<Block,CraftAnvil> anvils;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		this.anvils = new HashMap<Block,CraftAnvil>();
		
		getApi().buildTask("repair anvils", new HCTask() {
			
			@Override
			public void run() {
				repairAnvils();
			}
		})
		.withInterval(6000) // 5 minutes
		.addListener(new HCTaskListener(){
			
			@EventHandler
			public void afterEnd(HCBeforeEndEvent e){
				repairAnvils();
			}
		})
		.build()
		.start();

	}


	@SuppressWarnings("deprecation")
	private void repairAnvils() {
		for(Iterator<Map.Entry<Block,CraftAnvil>> it = anvils.entrySet().iterator(); it.hasNext(); ) {
	        Entry<Block,CraftAnvil> entry = it.next();
	        if(!entry.getKey().getType().equals(Material.ANVIL)){
				Block anvil = entry.getKey();
				anvil.setType(Material.ANVIL);
				CraftAnvil oldAnvilData = (CraftAnvil) anvil.getBlockData().clone();
				BlockFace facing = oldAnvilData.getFacing();
				switch (facing) {
					case NORTH:
						oldAnvilData.setFacing(BlockFace.SOUTH);
						break;
					case EAST:
						oldAnvilData.setFacing(BlockFace.WEST);
						break;
					case WEST:
						oldAnvilData.setFacing(BlockFace.EAST);
						break;
					case SOUTH:
						oldAnvilData.setFacing(BlockFace.NORTH);
						break;
					default:
						break;
				}
				anvil.setBlockData(oldAnvilData);
				it.remove();
			}
	    }
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onBlockExploser(BlockExplodeEvent e){
		e.setCancelled(true);
	}
	

	@EventHandler
	public void onInteractItemFrame(PlayerInteractEntityEvent e){
		if(e.getRightClicked().getType().equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}
	

	@EventHandler
	public void onDamageItemFrame(EntityDamageEvent e){
		if(e.getEntity().getType().equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e){
		e.setCancelled(true);
	}
	
    @SuppressWarnings("deprecation")
	@EventHandler
    public void onClickAnvil(PlayerInteractEvent event){
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if(event.getClickedBlock().getType() == Material.ANVIL){
                Block anvil = event.getClickedBlock();
				CraftAnvil oldAnvilData = (CraftAnvil) anvil.getBlockData().clone();
				BlockFace facing = oldAnvilData.getFacing();
				switch (facing) {
					case NORTH:
						oldAnvilData.setFacing(BlockFace.SOUTH);
						break;
					case EAST:
						oldAnvilData.setFacing(BlockFace.WEST);
						break;
					case WEST:
						oldAnvilData.setFacing(BlockFace.EAST);
						break;
					case SOUTH:
						oldAnvilData.setFacing(BlockFace.NORTH);
						break;
					default:
						break;
				}
				anvil.setBlockData(oldAnvilData);

                if(!anvils.containsKey(anvil)){
                    anvils.put(anvil,oldAnvilData);
                }
            }
        }
    }
    
    @EventHandler
    public void onWalkOnJumpPad(PlayerInteractEvent event){
    	if(event.getAction() == Action.PHYSICAL){
    		Block plate = event.getClickedBlock();
    		if(plate.getType().equals(Material.HEAVY_WEIGHTED_PRESSURE_PLATE)){
    			Block beneath = plate.getRelative(0, -1, 0);
				Player player = event.getPlayer();
    			if(!player.isDead() && beneath != null && Tag.WOOL.isTagged(beneath.getType())){
    		    	HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
    		    	if(hcPlayer != null){
    		    		Location dest = plate.getRelative(0, 5, 0).getLocation().add(0.5, 0, 0.5);
    		    		dest.setYaw(player.getLocation().getYaw());
    		    		dest.setPitch(player.getLocation().getPitch());
    		    		Vector velocity = player.getVelocity();
        				player.teleport(dest);
        				player.setVelocity(velocity);
        				getSoundApi().play(Sound.ENTITY_ENDERMAN_TELEPORT, dest, 1, 2);
        				event.setCancelled(true);
    		    	}
    			}
    		}
    	}
    }
	
}
