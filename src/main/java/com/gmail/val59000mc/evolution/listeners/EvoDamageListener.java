package com.gmail.val59000mc.evolution.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.evolution.callbacks.EvoCallbacks;
import com.gmail.val59000mc.evolution.common.EvoConstants;
import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.evolution.zones.ForbiddenZone;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByHCPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByPotionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;

public class EvoDamageListener extends HCListener{
	private Map<HCTeam, ForbiddenZone> zones;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		EvoCallbacks callbacks = ((EvoCallbacks) getCallbacksApi());
		zones = new HashMap<>();
		zones.put(getPmApi().getHCTeam("Rouge"), callbacks.getRedZone());
		zones.put(getPmApi().getHCTeam("Bleu"), callbacks.getBlueZone());
		
		EvoConstants.STARS_KILL = getApi().getConfig().getInt("stars.kill-own",5);
		EvoConstants.STARS_KILLED = getApi().getConfig().getInt("stars.death",2);
		EvoConstants.STARS_TEAM_KILL = getApi().getConfig().getInt("stars.kill-team",3);
		EvoConstants.KILLS_LIMIT = getApi().getConfig().getInt("kills-limit",100);
		
	}
	
	@EventHandler
	public void onPlayerDamaged(HCPlayerDamagedEvent e){
		EvoPlayer evoPlayer = ((EvoPlayer) e.getHcDamaged());
		if(evoPlayer.hasTeam() && evoPlayer.is(PlayerState.PLAYING)){
			ForbiddenZone zone = zones.get(evoPlayer.getTeam());
			if(zone.contains(evoPlayer.getPlayer())){
				e.getWrapped().setCancelled(true);
			}else{
				evoPlayer.refreshLastPVP();
			}
		}
	}
	
	@EventHandler
	public void onPlayerDamagedByPlayer(HCPlayerDamagedByHCPlayerEvent e){
		EvoPlayer evoDamaged = 	((EvoPlayer) e.getHcDamaged());
		EvoPlayer evoDamager = 	((EvoPlayer) e.getHcDamager());
		if(!evoDamaged.hasTeam() || !evoDamager.hasTeam() || !evoDamaged.is(PlayerState.PLAYING) || !evoDamager.is(PlayerState.PLAYING)){
			return;
		}
		
		ForbiddenZone zoneDamaged = zones.get(evoDamaged.getTeam());
		ForbiddenZone zoneDamager = zones.get(evoDamager.getTeam());
		if(zoneDamaged.contains(evoDamaged.getPlayer()) || zoneDamager.contains(evoDamager.getPlayer())){
			e.getWrapped().setCancelled(true);
		}else{
			evoDamaged.refreshLastPVP();
			evoDamager.refreshLastPVP();
		}
	}
	
	@EventHandler
	public void onPlayerDamagedByPotion(HCPlayerDamagedByPotionEvent e){
		EvoPlayer evoDamaged = 	((EvoPlayer) e.getHcDamaged());
		EvoPlayer evoDamager = 	((EvoPlayer) e.getHcDamager());
		if(!evoDamaged.hasTeam() || !evoDamager.hasTeam() || !evoDamaged.is(PlayerState.PLAYING) || !evoDamager.is(PlayerState.PLAYING)){
			return;
		}
		
		ForbiddenZone zoneDamaged = zones.get(evoDamaged.getTeam());
		ForbiddenZone zoneDamager = zones.get(evoDamager.getTeam());
		if(zoneDamaged.contains(evoDamaged.getPlayer()) || zoneDamager.contains(evoDamager.getPlayer())){
			e.getWrapped().setCancelled(true);
		}else{
			evoDamaged.refreshLastPVP();
			evoDamager.refreshLastPVP();
		}
	}
	
}
