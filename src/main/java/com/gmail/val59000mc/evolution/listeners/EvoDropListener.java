package com.gmail.val59000mc.evolution.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;

public class EvoDropListener extends HCListener{

	/**
	 * No drop while not playing
	 * No drops of base items (leather, wood, arrow)
	 * @param e
	 */
	@EventHandler
	public void onDropItem(PlayerDropItemEvent e){
		switch(getApi().getGameState()){
			case INITIALIZING:
			case LOADING:
			case WAITING:
			case STARTING:
				e.setCancelled(true);
				break;
			case PLAYING:
				switch(e.getItemDrop().getItemStack().getType()){
					case ARROW:
						
					case WOODEN_SWORD:
					case IRON_SWORD:
					case DIAMOND_SWORD:
						
					case LEATHER_HELMET:
					case LEATHER_CHESTPLATE:
					case LEATHER_LEGGINGS:
					case LEATHER_BOOTS:
						
					case GOLDEN_HELMET:
					case GOLDEN_CHESTPLATE:
					case GOLDEN_LEGGINGS:
					case GOLDEN_BOOTS:
						
					case IRON_HELMET:
					case IRON_CHESTPLATE:
					case IRON_LEGGINGS:
					case IRON_BOOTS:
						
					case DIAMOND_HELMET:
					case DIAMOND_CHESTPLATE:
					case DIAMOND_LEGGINGS:
					case DIAMOND_BOOTS:					
						e.setCancelled(true);
						break;
					default:
						break;
				}
				break;
			case ENDED:
				e.setCancelled(true);
				break;
			default:
				break;
			
		}
	}
}
