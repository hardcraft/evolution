package com.gmail.val59000mc.evolution.listeners;

import com.gmail.val59000mc.evolution.callbacks.EvoCallbacks;
import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.evolution.zones.ForbiddenZone;
import com.gmail.val59000mc.hcgameslib.events.*;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.HashMap;
import java.util.Map;

import static com.gmail.val59000mc.evolution.common.EvoConstants.MAX_CAMPING_SECONDS;

public class EvoForbiddenZoneListener extends HCListener{

    private Map<HCTeam, ForbiddenZone> zones;

	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
        EvoCallbacks callbacks = ((EvoCallbacks) getCallbacksApi());
        zones = new HashMap<>();
        zones.put(getPmApi().getHCTeam("Rouge"), callbacks.getRedZone());
        zones.put(getPmApi().getHCTeam("Bleu"), callbacks.getBlueZone());
        MAX_CAMPING_SECONDS =  getConfig().getInt("max-camping-seconds", MAX_CAMPING_SECONDS);
	}
	
	@EventHandler
	public void afterPlay(HCAfterPlayEvent e){
		
        startKillAndPushZones();
        startAntiCampZones();
		
	}

    private void startKillAndPushZones() {
        getApi().buildTask("kill and push zones", new HCTask() {

            @Override
            public void run() {
                for(ForbiddenZone zone : zones.values()){
                    zone.check();
                }
            }

        })
        .withInterval(4)
        .addListener(new HCTaskListener(){

            @EventHandler
            public void afterEnd(HCBeforeEndEvent e){
                getScheduler().stop();
            }
        })
        .build()
        .start();
    }


    private void startAntiCampZones() {
        getApi().buildTask("anti camp", new HCTask() {

            @Override
            public void run() {
                for(HCPlayer hcPlayer : getPmApi().getPlayers(true, PlayerState.PLAYING)){
                    ForbiddenZone zone = zones.get(hcPlayer.getTeam());
                    if(zone != null){
                        EvoPlayer evoPlayer = (EvoPlayer) hcPlayer;
                        Player player = evoPlayer.getPlayer();
                        if(zone.contains(player))
                            evoPlayer.increaseCampingSeconds();
                        else
                            evoPlayer.decreaseCampingSeconds();

                        if(evoPlayer.getCampingSeconds() >= MAX_CAMPING_SECONDS){
                            player.kickPlayer(getStringsApi().get("evo.camping-kick-reason").toString());
                            getStringsApi().get("evo.camping-kick").sendChatP();
                        }
                    }
                }

                getPmApi().updatePlayersScoreboards();
            }

        })
        .withInterval(20)
        .addListener(new HCTaskListener(){

            @EventHandler
            public void onJoin(HCPlayerJoinEvent e){
                ((EvoPlayer) e.getHcPlayer()).resetCampingSeconds();
            }

            @EventHandler
            public void onRespawn(HCPlayerRespawnEvent e){
                ((EvoPlayer) e.getHcPlayer()).resetCampingSeconds();
            }

            @EventHandler
            public void afterEnd(HCBeforeEndEvent e){
                getScheduler().stop();
            }
        })
        .build()
        .start();
    }


}
