package com.gmail.val59000mc.evolution.listeners;

import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;

public class EvoMySQLListener extends HCListener{

	// Queries
	private String createEvoPlayerSQL;
	private String createEvoPlayerStatsSQL;
	private String createEvoPlayerPerksSQL;
	
	private String insertEvoPlayerSQL;
	private String insertEvoPlayerStatsSQL;
	private String insertEvoPlayerPerksSQL;
	
	private String updateEvoPlayerGlobalStatsSQL;
	private String updateEvoPlayerStatsSQL;
	
	private String selectEvoPlayerPerksSQL;
	
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			createEvoPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_evo_player.sql");
			createEvoPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_evo_player_stats.sql");
			createEvoPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_evo_player_perks.sql");
			
			insertEvoPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_evo_player.sql");
			insertEvoPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_evo_player_stats.sql");
			insertEvoPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_evo_player_perks.sql");
			
			updateEvoPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/update_evo_player_global_stats.sql");
			updateEvoPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/update_evo_player_stats.sql");
			selectEvoPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/select_evo_player_perks.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					try{
						sql.execute(sql.prepareStatement(createEvoPlayerSQL));
						
						sql.execute(sql.prepareStatement(createEvoPlayerStatsSQL));
						
						sql.execute(sql.prepareStatement(createEvoPlayerPerksSQL));
						
					}catch(SQLException e){
						Logger.severe("Couldnt create tables for Evolution stats or perks");
						e.printStackTrace();
					}
				
				}
			});
		}
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerInserted(HCPlayerDBInsertedEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					String id = String.valueOf(e.getHcPlayer().getId());

					try {
						// Insert ctf player if not exists
						sql.execute(sql.prepareStatement(insertEvoPlayerSQL, id));
						
						sql.execute(sql.prepareStatement(insertEvoPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
						
						sql.execute(sql.prepareStatement(insertEvoPlayerPerksSQL, id));
					
						CachedRowSet perks = sql.query(sql.prepareStatement(selectEvoPlayerPerksSQL, id));
						perks.first();
						EvoPlayer evoPlayer = (EvoPlayer) e.getHcPlayer();
						evoPlayer.setDoubleStarsChance(perks.getInt("star"));
					} catch (SQLException e1) {
						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
					}
				}
			});
		}
	}
	
	
	/**
	 * Save all players global data when game ends
	 * @param e
	 */
	@EventHandler
	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			EvoPlayer evoPlayer = (EvoPlayer) e.getHcPlayer();
			
			// Launch one async task to save all data
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
						// Update evo player global stats
						sql.execute(sql.prepareStatement(updateEvoPlayerGlobalStatsSQL,
								String.valueOf(evoPlayer.getStars()),
								String.valueOf(evoPlayer.getTimePlayed()),
								String.valueOf(evoPlayer.getKills()),
								String.valueOf(evoPlayer.getDeaths()),
								String.valueOf(evoPlayer.getMoney()),
								String.valueOf(evoPlayer.getWins()),
								String.valueOf(e.getHcPlayer().getId())
						));
						
						// Update evo player stats
						sql.execute(sql.prepareStatement(updateEvoPlayerStatsSQL,
								String.valueOf(evoPlayer.getStars()),
								String.valueOf(evoPlayer.getTimePlayed()),
								String.valueOf(evoPlayer.getKills()),
								String.valueOf(evoPlayer.getDeaths()),
								String.valueOf(evoPlayer.getMoney()),
								String.valueOf(evoPlayer.getWins()),
								String.valueOf(e.getHcPlayer().getId()),
								sql.getMonth(),
								sql.getYear()
						));
					

					
					}catch(SQLException e){
						Logger.severe("Couldnt update Evolution player stats for player="+evoPlayer.getName()+" uuid="+evoPlayer.getUuid());
						e.printStackTrace();
					}
					
				}
			});
		}

	}
}
