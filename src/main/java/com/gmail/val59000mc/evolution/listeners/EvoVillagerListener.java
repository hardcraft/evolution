package com.gmail.val59000mc.evolution.listeners;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

import com.gmail.val59000mc.evolution.common.EvoInventories;
import com.gmail.val59000mc.evolution.common.VillagerShop;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterInventoryEvent;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterTriggerEvent;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.triggers.RightClickEntityUUIDTrigger;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerManager;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Parser;
import com.google.common.collect.Lists;

public class EvoVillagerListener extends HCListener{

	private VillagerShop redShop;
	private VillagerShop blueShop;

	@EventHandler
	public void onVillagerDamage(EntityDamageEvent e){
		if(e.getEntity().getType().equals(EntityType.VILLAGER)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){

		// Remove villagers and items on ground
		World world = getApi().getWorldConfig().getWorld();
		Chunk chunk = world.getSpawnLocation().getChunk();
		for(int i=-4 ; i<4 ;i++){
			for(int j=-4 ; j<4 ;j++){
				world.getChunkAt(chunk.getX()+i, chunk.getZ()+j).load();
			}
		}
		Iterator<Entity> it = world.getEntities().iterator();
		while(it.hasNext()){
			Entity entity = it.next();
			if(entity.getType().equals(EntityType.VILLAGER) || entity.getType().equals(EntityType.DROPPED_ITEM)){
				entity.remove();
			}
		}


		// Create new villagers
		Log.debug("creating villagers");
		redShop = createVillagerShop(getApi().getConfig().getConfigurationSection("teams.red.villagers"), getPmApi().getHCTeam("Rouge"));
		blueShop = createVillagerShop(getApi().getConfig().getConfigurationSection("teams.blue.villagers"), getPmApi().getHCTeam("Bleu"));

		Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable() {
			public void run() {
				redShop.setNoSound();
				blueShop.setNoSound();
			}
		}, 20);
	}

	@EventHandler
	public void onSIGRegisterInventories(SIGRegisterInventoryEvent e){

		HCStringsAPI s = getStringsApi();

		// register potion
		InventoryManager im = e.getIm();

		EvoInventories i = new EvoInventories(getApi());

		// Potion inventory
		Inventory potion = new Inventory("evo-potion", "§a"+s.get("evo.potion").toString(), 1);
		potion.addItem(i.getGlass(0));
		potion.addItem(i.getGlass(1));
		potion.addItem(i.getGoldenApple(2, 2));
		potion.addItem(i.getLeapPotion(3, 3));
		potion.addItem(i.getSpeedPotion(4, 4));
		potion.addItem(i.getHealPotion(5, 5));
		potion.addItem(i.getDamagePotion(6, 6));
		potion.addItem(i.getGlass(7));
		potion.addItem(i.getGlass(7));
		im.addInventory(potion);

		// Armor inventory
		Inventory armor = new Inventory("evo-armor", "§a"+s.get("evo.armor").toString(), 4);
		armor.addItem(i.getGlass(0));
		armor.addItem(i.getGlass(1));
		armor.addItem(i.getGlass(2));
		armor.addItem(i.getGoldHelmet(3,10));
		armor.addItem(i.getIronHelmet(4,20));
		armor.addItem(i.getDiamondHelmet(5,30));
		armor.addItem(i.getGlass(6));
		armor.addItem(i.getGlass(7));
		armor.addItem(i.getGlass(8));

		armor.addItem(i.getGlass(9));
		armor.addItem(i.getGlass(10));
		armor.addItem(i.getGlass(11));
		armor.addItem(i.getGoldChestplate(12,10));
		armor.addItem(i.getIronChestplate(13,20));
		armor.addItem(i.getDiamondChestplate(14,30));
		armor.addItem(i.getGlass(15));
		armor.addItem(i.getGlass(16));
		armor.addItem(i.getGlass(17));

		armor.addItem(i.getGlass(18));
		armor.addItem(i.getGlass(19));
		armor.addItem(i.getGlass(20));
		armor.addItem(i.getGoldLeggings(21,10));
		armor.addItem(i.getIronLeggings(22,20));
		armor.addItem(i.getDiamondLeggings(23,30));
		armor.addItem(i.getGlass(24));
		armor.addItem(i.getGlass(25));
		armor.addItem(i.getGlass(26));

		armor.addItem(i.getGlass(27));
		armor.addItem(i.getGlass(28));
		armor.addItem(i.getGlass(29));
		armor.addItem(i.getGoldBoots(30,10));
		armor.addItem(i.getIronBoots(31,20));
		armor.addItem(i.getDiamondBoots(32,30));
		armor.addItem(i.getGlass(33));
		armor.addItem(i.getGlass(34));
		armor.addItem(i.getGlass(35));
		im.addInventory(armor);

		// Weapon inventory
		Inventory weapon = new Inventory("evo-weapon", "§a"+s.get("evo.weapon").toString(), 1);
		weapon.addItem(i.getGlass(0));
		weapon.addItem(i.getGlass(1));
		weapon.addItem(i.getStoneSword(2, 10));
		weapon.addItem(i.getIronSword(3, 20));
		weapon.addItem(i.getDiamondSword(4, 30));
		weapon.addItem(i.getGlass(5));
		weapon.addItem(i.getBow(6, 20));
		weapon.addItem(i.getGlass(7));
		weapon.addItem(i.getGlass(8));
		im.addInventory(weapon);


		// Weapon inventory
		Inventory enchant = new Inventory("evo-enchant", "§a"+s.get("evo.enchant").toString(), 1);
		enchant.addItem(i.getGlass(0));
		enchant.addItem(i.getProtection(1, 20));
		enchant.addItem(i.getGlass(2));
		enchant.addItem(i.getSharpness(3, 20));
		enchant.addItem(i.getGlass(4));
		enchant.addItem(i.getPower(5, 20));
		enchant.addItem(i.getGlass(6));
		enchant.addItem(i.getFeatherFalling(7, 20));
		enchant.addItem(i.getGlass(8));
		im.addInventory(enchant);
	}

	@EventHandler
	public void onSIGRegisterTriggers(SIGRegisterTriggerEvent e){
		TriggerManager tm = e.getTm();
		Log.debug("registering villager triggers");
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-potion")), redShop.getPotion().getUniqueId()));
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-armor")), redShop.getArmor().getUniqueId()));
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-weapon")), redShop.getWeapon().getUniqueId()));
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-enchant")), redShop.getEnchant().getUniqueId()));

		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-potion")), blueShop.getPotion().getUniqueId()));
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-armor")), blueShop.getArmor().getUniqueId()));
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-weapon")), blueShop.getWeapon().getUniqueId()));
		tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, null, Lists.newArrayList(new OpenInventoryAction("evo-enchant")), blueShop.getEnchant().getUniqueId()));

	}

	private VillagerShop createVillagerShop(ConfigurationSection section, HCTeam hcTeam){
		World world = getApi().getWorldConfig().getWorld();

		Location potionLoc = Parser.parseLocation(world, section.getString("potion"));
		Location armorLoc = Parser.parseLocation(world, section.getString("armor"));
		Location weaponLoc = Parser.parseLocation(world, section.getString("weapon"));
		Location enchantLoc = Parser.parseLocation(world, section.getString("enchant"));

		VillagerShop shop = new VillagerShop(
			world.spawnEntity(potionLoc, EntityType.VILLAGER),
			world.spawnEntity(armorLoc, EntityType.VILLAGER),
			world.spawnEntity(weaponLoc, EntityType.VILLAGER),
			world.spawnEntity(enchantLoc, EntityType.VILLAGER),
			hcTeam
		);

		Log.debug(hcTeam.getName()+" potion villager spawned at "+Locations.printLocation(shop.getPotion().getLocation()));
		Log.debug(hcTeam.getName()+" armor villager spawned at "+Locations.printLocation(shop.getArmor().getLocation()));
		Log.debug(hcTeam.getName()+" weapon villager spawned at "+Locations.printLocation(shop.getWeapon().getLocation()));
		Log.debug(hcTeam.getName()+" enchant villager spawned at "+Locations.printLocation(shop.getEnchant().getLocation()));

		shop.clearTrades();
		shop.updateNames();
		shop.setProfessions();

		return shop;
	}
}
