package com.gmail.val59000mc.evolution.players;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.entity.Player;

public class EvoPlayer extends HCPlayer{

	private int stars;
	private int doubleStarsChance;
	private long lastPVP;
	private long campingSeconds;
	
	public EvoPlayer(Player player) {
		super(player);
		this.stars = 0;
		this.doubleStarsChance = 0;
		this.lastPVP = 0;
		this.campingSeconds = 0;
	}
	
	public EvoPlayer(HCPlayer hcPlayer){
		super(hcPlayer);
		EvoPlayer evoPlayer = (EvoPlayer) hcPlayer;
		this.stars = evoPlayer.stars;
		this.doubleStarsChance = evoPlayer.doubleStarsChance;
	}
	
	public void subtractBy(HCPlayer lastUpdatedSession) {
		super.subtractBy(lastUpdatedSession);
		EvoPlayer evoPlayer = (EvoPlayer) lastUpdatedSession;
		this.stars -= evoPlayer.stars;
	}

	public long remainingSecondsBeforeBackToBase(){
		long remaining = System.currentTimeMillis() - this.lastPVP;
		if(remaining <= 4000){
			return ((4000-remaining)/1000);
		}else{
			return 0;
		}
	}
	
	public void refreshLastPVP(){
		this.lastPVP = System.currentTimeMillis();
	}
	
	public void resetLastPVP(){
		this.lastPVP = 0;
	}
	
	public void calculatePerks(){
	}

	public int getStars() {
		return stars;
	}

	public int getDoubleStarsChance() {
		return doubleStarsChance;
	}
	
	public void setDoubleStarsChance(int doubleStarsChance) {
		this.doubleStarsChance = doubleStarsChance;
	}

	public void addStars(int amount){
		this.stars += amount;
	}

	public void increaseCampingSeconds(){
		campingSeconds++;
	}

	public void decreaseCampingSeconds(){
		if(campingSeconds > 0)
			campingSeconds--;
	}

    public void resetCampingSeconds(){
        campingSeconds = 0;
    }

    public long getCampingSeconds() {
        return campingSeconds;
    }
}
