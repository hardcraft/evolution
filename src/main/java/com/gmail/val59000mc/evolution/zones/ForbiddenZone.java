package com.gmail.val59000mc.evolution.zones;

import java.util.List;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.spigotutils.Time;

public class ForbiddenZone {
	private HCTeam team; // forbidden team
	private HCGameAPI api;
	private List<PushZone> pushZones;
	private List<KillZone> killZones;
	
	
	public ForbiddenZone(HCGameAPI api, List<PushZone> pushZones, List<KillZone> killZones, HCTeam team) {
		super();
		this.api = api;
		this.pushZones = pushZones;
		this.killZones = killZones;
		this.team = team;
	}
	
	public boolean contains(Player player){
		for(PushZone pushzone : pushZones){
			if(pushzone.contains(player)){
				return true;
			}
		}
		
		for(KillZone killZone : killZones){
			if(killZone.contains(player)){
				return true;
			}
		}
		
		return false;
	}

	public void check(){
		for(HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(true, PlayerState.PLAYING)){
			
			EvoPlayer evoPlayer = (EvoPlayer) hcPlayer;
			
			for(PushZone pushzone : pushZones){
				if(pushzone.checkForbiddenTeam(evoPlayer, team)){
					api.getStringsAPI().get("evo.cannot-attack-enemy-spawn").sendChatP(hcPlayer);
					api.getSoundAPI().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 0.5f, 2);
				}
				else if(pushzone.checkAllowedTeam(evoPlayer, team)){
					api.getStringsAPI()
						.get("evo.wait-before-enter-spawn")
						.replace("%time%", Time.getFormattedTime(evoPlayer.remainingSecondsBeforeBackToBase()))
						.sendChatP(hcPlayer);
					api.getSoundAPI().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 0.5f, 2);
				}
			}
			
			for(KillZone killZone : killZones){
				if(killZone.checkForbiddenTeam(evoPlayer, team)){
					api.getStringsAPI().get("evo.cannot-attack-enemy-spawn").sendChatP(hcPlayer);
					api.getSoundAPI().play(hcPlayer, Sound.ENTITY_PLAYER_HURT, 1, 1);
				}
			}
		}
	}
}
