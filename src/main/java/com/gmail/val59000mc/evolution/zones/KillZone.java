package com.gmail.val59000mc.evolution.zones;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;

public class KillZone {

	private LocationBounds zone;

	public KillZone(LocationBounds zone) {
		super();
		this.zone = zone;
	}
	
	public boolean contains(Player player){
		return zone.contains(player.getLocation());
	}
	
	public boolean checkForbiddenTeam(EvoPlayer evoPlayer, HCTeam forbiddenTeam){
		if(evoPlayer.hasTeam() && evoPlayer.getTeam().equals(forbiddenTeam)){
			Player player = evoPlayer.getPlayer();
			if(zone.contains(player.getLocation()) && !player.isDead()){
				player.setHealth(0);
				return true;
			}
		}
		return false;
	}
	
}
