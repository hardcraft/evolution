package com.gmail.val59000mc.evolution.zones;


import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.evolution.players.EvoPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;

public class PushZone {
	private LocationBounds zone;
	private Vector push;
	
	public PushZone(LocationBounds zone, Vector push){
		super();
		this.zone = zone;
		this.push = push;
	}
	
	public boolean contains(Player player){
		return zone.contains(player.getLocation());
	}
	
	public boolean checkForbiddenTeam(EvoPlayer evoPlayer, HCTeam forbiddenTeam){
		if(evoPlayer.hasTeam() && evoPlayer.getTeam().equals(forbiddenTeam)){
			Player player = evoPlayer.getPlayer();
			if(zone.contains(player.getLocation())){
				player.setVelocity(push);
				if(player.getHealth() > 2)
					player.damage(2);
				return true;
			}
		}
		return false;
	}
	
	public boolean checkAllowedTeam(EvoPlayer evoPlayer, HCTeam forbiddenTeam){
		if(evoPlayer.hasTeam() && !evoPlayer.getTeam().equals(forbiddenTeam)){
			Player player = evoPlayer.getPlayer();
			if(evoPlayer.remainingSecondsBeforeBackToBase() > 0 && zone.contains(player.getLocation())){
				player.setVelocity(push);
				return true;
			}
		}
		return false;
	}
}
